using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	public int _Score = 0;
	public GUIText ScoreText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ScoreText.text = "Score : " + _Score.ToString();
	}
}
