using UnityEngine;
using System.Collections;


public class Window : MonoBehaviour {
	public Score m_score;
	
	public GUIText GameTime;
	public Texture Badman;
	public Texture Goodman;
	public Texture Nonman;

	float ChangeGab;
	float GameGab;
	public int State = 0;
	bool CheckGame;
	// Use this for initialization
	void Start () 
	{
		ChangeGab = Time.time + 1;
		GameGab = Time.time + 10;
		CheckGame = true;
		Setting ();
	}
	
	// Update is called once per frame
	void Update () {
		GameTime.text = "Time : " + (10-Time.time);
		ChangeMan();
		EndGame();
	}
	void Setting()
	{
		int i = Random.Range(0,3);
		
		switch(i)
		{
		case 0:
			renderer.material.mainTexture = Badman;
			State = 1;
			break;
		case 1:
			renderer.material.mainTexture = Goodman;
			State = 2;
			break;
		case 2:
			renderer.material.mainTexture = Nonman;
			State = 3;
			break;
		}
	}
	void ChangeMan()
	{
		if(Time.time > ChangeGab)
		{
			ChangeGab += 1;
			
			int i = Random.Range(0,3);
		
			switch(i)
			{
			case 0:
				renderer.material.mainTexture = Badman;
				State = 1;
				break;
			case 1:
				renderer.material.mainTexture = Goodman;
				State = 2;
				break;
			case 2:
				renderer.material.mainTexture = Nonman;
				State = 3;
				break;
			}
		}
	}
	
	void EndGame()
	{
		
		if(Time.time > GameGab)
		{
			float gab = GameGab + 3;
			GameTime.text = "GameOver";
			CheckGame = false;
			
			if(Time.time > gab)
			{
				Application.LoadLevel("Title");
			}
		}
	}
	void OnMouseDown()
	{
		if(true == CheckGame)
		{
			switch(State)
			{
			case 1:
				m_score._Score += 1;		
				break;
			case 2:
				m_score._Score -= 2;
				break;
			case 3:
				m_score._Score -= 1;
				break;
			}	
			renderer.material.mainTexture = null;
			State = 0;
		}
	}
}
