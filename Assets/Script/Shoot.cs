using UnityEngine;
using System.Collections;


public class Shoot : MonoBehaviour {

	public state m_state;
	public GUIText score;
	int Score = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnMouseDown()
	{
		switch(m_state.State)
		{
		case 1:
			++Score;
			break;
		case 2:
			--Score;
			break;
		}		
		++Score;
		score.text = Score.ToString();
	}
}
