using UnityEngine;
using System.Collections;

public class state : MonoBehaviour {
	enum eState{ BAD = 1 , GOOD = 2 , NON = 3};
	public Texture Badman;
	public Texture Goodman;
	public Texture Nonman;
	
	public int State;
	// Use this for initialization
	void Start () {
		Setting();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void Setting()
	{
		int i = Random.Range(0,3);
		
		switch(i)
		{
		case 0:
			renderer.material.mainTexture = Badman;
			State = (int)eState.BAD;
			break;
		case 1:
			renderer.material.mainTexture = Goodman;
			State = (int)eState.GOOD;
			break;
		case 2:
			renderer.material.mainTexture = Nonman;
			State = (int)eState.NON;
			break;
		}
	}
}
